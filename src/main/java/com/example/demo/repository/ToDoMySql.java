package com.example.demo.repository;

import com.example.demo.entity.ToDo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Repository
@Qualifier("sql")
public class ToDoMySql implements ToDoDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ToDoMySql.class.getName());

    @Autowired
    DataSource dataSource;

    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<ToDo> getAll() {
        List<ToDo> toDoList = new ArrayList<>();

        try {
            con = dataSource.getConnection();
            ps = con.prepareStatement("SELECT * FROM todos_table;");
            rs = ps.executeQuery();

            while (rs.next()) {
                int itemId = rs.getInt("id");
                String itemAprasymas = rs.getString("aprasymas");
                LocalDate itemDate = rs.getDate("data").toLocalDate();
                Boolean pozymis = rs.getBoolean("pozymis");
                ToDo toDoItem = new ToDo(itemId, itemAprasymas, itemDate, pozymis);
                toDoList.add(toDoItem);
            }

        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        } finally {

            try {
                if (rs != null || ps != null || con != null) {

                    rs.close();
                    ps.close();
                    con.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.toString(), e);
            }
        }
        return toDoList;
    }

    public ToDo getToDoById(int id) {
        ToDo td = null;
        try {
            con = dataSource.getConnection();
            ps = con.prepareStatement("SELECT * FROM todos_table where id = ?;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                int itemId = rs.getInt("id");
                String itemAprasymas = rs.getString("aprasymas");
                LocalDate itemDate = rs.getDate("data").toLocalDate();
                Boolean pozymis = rs.getBoolean("pozymis");
                ToDo toDoItem = new ToDo(itemId, itemAprasymas, itemDate, pozymis);
                td = toDoItem;
            }

        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);

        } finally {
            try {
                if (ps != null || con != null) {
                    ps.close();
                    con.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.toString(), e);
            }
        }

        return td;
    }

    public ToDo addToDo(ToDo toDo) {

        try {
            con = dataSource.getConnection();
            ps = con.prepareStatement("INSERT INTO todos_table (id, aprasymas, data, pozymis) VALUES(?, ?, ?, ?);");
            ps.setInt(1, toDo.getId());
            ps.setString(2, toDo.getAprasymas());
            ps.setDate(3, Date.valueOf(toDo.getData()));
            ps.setBoolean(4, toDo.isPozymis());
            ps.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        } finally {
            try {
                if (ps != null || con != null) {
                    ps.close();
                    con.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.toString(), e);
            }
        }
        return toDo;
    }

    public int updateToDo(ToDo toDo, int id) {
        try {
            con = dataSource.getConnection();
            ps = con.prepareStatement("UPDATE todos_table SET aprasymas=?, data=?, pozymis=? WHERE id=?;");
            ps.setString(1, toDo.getAprasymas());
            ps.setDate(2, Date.valueOf(toDo.getData()));
            ps.setBoolean(3, toDo.isPozymis());
            ps.setInt(4, toDo.getId());
            ps.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        } finally {
            try {
                if (ps != null || con != null) {
                    ps.close();
                    con.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.toString(), e);
            }
        }
        return toDo.getId();
    }

    public void deleteToDo(int id) {
        try {
            con = dataSource.getConnection();
            ps = con.prepareStatement("DELETE FROM todos_table WHERE id=?;");
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.toString(), e);
        } finally {
            try {
                if (ps != null || con != null) {
                    ps.close();
                    con.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.toString(), e);
            }
        }
    }
}
