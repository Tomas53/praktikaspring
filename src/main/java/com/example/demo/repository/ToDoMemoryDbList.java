package com.example.demo.repository;

import com.example.demo.entity.ToDo;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Repository
@Qualifier("inMemory")
public class ToDoMemoryDbList implements ToDoDao {

    private List<ToDo> toDoList = new ArrayList<>();

    public List<ToDo> getAll() {
        return toDoList;
    }

    public ToDo getToDoById(int id) {
        ToDo toDo = null;
        for (ToDo toDo1 : toDoList) {
            if (toDo1.getId() == (id)) {
                toDo = toDo1;
            }
        }
        return toDo;
    }

    public ToDo addToDo(ToDo toDo) {

        toDoList.add(toDo);
        return toDo;
    }

    public int updateToDo(ToDo toDo, int id) {

        for (ToDo toDo1 : toDoList) {
            if (toDo1.getId() == (id)) {
                if (!toDo1.getAprasymas().equals(toDo.getAprasymas())) {
                    toDo1.setAprasymas(toDo.getAprasymas());
                }
                if (toDo1.getData() != toDo.getData()) {
                    toDo1.setData(toDo.getData());
                }
                if (!toDo1.isPozymis() && toDo.isPozymis()) {
                    toDo1.setPozymis(true);
                }
            }
        }

        return toDo.getId();
    }

    public void deleteToDo(int id) {
        ListIterator<ToDo> iter = toDoList.listIterator();
        while (iter.hasNext()) {
            if (iter.next().getId() == (id)) {
                iter.remove();
            }
        }
    }

}
