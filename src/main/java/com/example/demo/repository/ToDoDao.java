package com.example.demo.repository;

import com.example.demo.entity.ToDo;


import java.util.List;

public interface ToDoDao {

    List<ToDo> getAll();

    ToDo getToDoById(int id);

    ToDo addToDo(ToDo toDo);

    int updateToDo(ToDo toDo, int id);

    void deleteToDo(int id);

}
