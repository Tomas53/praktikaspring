package com.example.demo.service;

import com.example.demo.entity.ToDo;
import com.example.demo.repository.ToDoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;

import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;

@Service
public class ToDoService {


    private ToDoDao toDoDao;

    @Autowired
    public ToDoService(@Qualifier("sql") ToDoDao toDoDao) {
        this.toDoDao = toDoDao;
    }

    public List<ToDo> getAllToDoList() {
        return toDoDao.getAll();
    }

    public ToDo getToDo(int id) {
        return toDoDao.getToDoById(id);
    }

    public ToDo addToDo(ToDo toDo) {
        if (toDo.getAprasymas().isEmpty() || toDo.getData() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The parameters must not be null or empty");
        }

        if (!toDo.getData().isAfter(LocalDate.now())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ToDo date is not valid");
        }
        if (toDo.isPozymis()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ToDo can't be true when created");
        }

        for (ToDo toDo1 : toDoDao.getAll()) {
            if (toDo1.getId() == (toDo.getId())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "id exists");
            }
        }
        return toDoDao.addToDo(toDo);
    }

    public int updateToDo(ToDo toDo, int id) {
        if (toDoDao.getAll().isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "No ToDo's to update");
        }
        if (toDo.getId() == 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The parameters must not be null or empty");
        }
        if (toDo.getId() != (id)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cant change id");
        }
        if (!toDo.getData().isAfter(LocalDate.now())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ToDo date is not valid");
        }

        int idCounter = 0;
        for (ToDo toDo1 : toDoDao.getAll()) {
            if (toDo1.getId() == (id)) {
                idCounter++;
            }
        }
        if (idCounter == 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "You cant change id");
        }
        return toDoDao.updateToDo(toDo, id);
    }

    public void deleteToDo(int id) {
        toDoDao.deleteToDo(id);
    }
}

