package com.example.demo.controller;

import com.example.demo.entity.ToDo;
import com.example.demo.service.ToDoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ToDoController {
    Logger logger = LoggerFactory.getLogger(ToDoController.class);

    private ToDoService toDoService;

    public ToDoController(ToDoService toDoService) {
        this.toDoService = toDoService;
    }

    @GetMapping(value = "/todos")
//   @CrossOrigin(origins = "http://localhost:3000")
    //@CrossOrigin(origins = "http://172.27.197.46:3000/")
    public List<ToDo> getAllToDo() {

        return toDoService.getAllToDoList();
    }

    @GetMapping(value = "/todos/{id}")
//    @CrossOrigin(origins = "http://localhost:3000")
    public ToDo getToDo(@PathVariable("id") int id) {

        return toDoService.getToDo(id);
    }

    @PostMapping(value = "/todos")
//    @CrossOrigin(origins = "http://localhost:3000")
    public ToDo addTodo(@RequestBody ToDo toDo) {

        return toDoService.addToDo(toDo);
    }

    @PutMapping(value = "/todos/{id}")
//    @CrossOrigin(origins = "http://localhost:3000")
    public int updateTodo(@RequestBody ToDo toDo, @PathVariable("id") int id) {

        return toDoService.updateToDo(toDo, id);
    }

    @DeleteMapping(value = "/todos/{id}")
//    @CrossOrigin(origins = "http://localhost:3000")
    public void deleteTodo(@PathVariable("id") int id) {

        toDoService.deleteToDo(id);
    }
}


