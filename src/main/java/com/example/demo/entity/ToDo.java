package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class ToDo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String aprasymas;
    private LocalDate data;
    private boolean pozymis;

    public ToDo(int id, String aprasymas, LocalDate data, boolean pozymis) {
        this.id = id;
        this.aprasymas = aprasymas;
        this.data = data;
        this.pozymis = pozymis;
    }

    public String getAprasymas() {
        return aprasymas;
    }

    public void setAprasymas(String aprasymas) {
        this.aprasymas = aprasymas;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPozymis() {
        return pozymis;
    }

    public void setPozymis(boolean pozymis) {
        this.pozymis = pozymis;
    }
}
